/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance;

/**
 *
 * @author Maintory_
 */
public class Dog extends animal {
    public Dog(String name, String color){
        super(name , color , 4);
        System.out.println("Dog Created");
    }
    @Override
    public void walk(){
        System.out.println("Dog: "+name + " walking with " + numberOfLegs + "legs");
    }
    @Override
    public void speak(){
        System.out.println("Dog: "+name + " Speaking > Hong Hong !! ");
    }
}
