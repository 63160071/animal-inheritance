/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance;

/**
 *
 * @author Maintory_
 */
public class Cat extends animal{
    public Cat(String name,String color){
    super(name, color,4);
}
    @Override
    public void walk(){
        System.out.println("CAT: "+name + " walking with " + numberOfLegs + "Legs");
    }
    @Override
    public void speak(){
        System.out.println("CAT: "+name + " Speaking > Meow >3< !! ");
    }
}