/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance;

/**
 *
 * @author Maintory_
 */
public class TestAnimal {
    public static void main(String[] args) {
        animal Animal = new animal("Ani","Red" ,0);
        Animal.speak();
        Animal.walk();
        
        Dog dog = new Dog("Dang", "Black and white");
        dog.speak();
        dog.walk();
        Cat cat = new Cat("Zero", "Orange");
        cat.speak();
        cat.walk();
        Duck duck = new Duck("Zom", "Orange");
        duck.speak();
        duck.walk();
        duck.fly();
    }
}
