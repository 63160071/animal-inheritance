/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance;

/**
 *
 * @author Maintory_
 */
public class Duck extends animal {
    private int numberOfWings;
    public Duck(String name,String color){
    super(name, color,2);
        System.out.println("Duck Created");
}
    public void fly(){
        System.out.println("name:" + this.name + " color:" + this.color + 
                " Legs:" + this.numberOfLegs + "FLYING !");
    }
    @Override
    public void walk(){
        System.out.println("Duck: "+name + "walking with " + numberOfLegs);
    }
    @Override
    public void speak(){
        System.out.println("Duck: "+name + "Speaking > Quack Quack !! ");
    }
}
