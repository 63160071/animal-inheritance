/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lnw.inheritance;

/**
 *
 * @author Maintory_
 */
public class animal {
    protected String name;
    protected int numberOfLegs;
    protected String color;
    public animal(String name,String color,int numberOfLegs){
        System.out.println("Animal Created !");
        this.name = name;
        this.numberOfLegs = numberOfLegs;
        this.color = color;
    }
    public void walk(){
        System.out.println("Animal Walk");
    }
    public void speak(){
        System.out.println("Animal Spoken");
        System.out.println("name:" + this.name + " color:" + this.color + 
                " Legs:" + this.numberOfLegs);
    }

    public String getName() {
        return name;
    }

    public int getNumberOfLegs() {
        return numberOfLegs;
    }

    public String getColor() {
        return color;
    }
    
}
